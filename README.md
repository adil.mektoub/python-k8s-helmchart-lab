# Introduction
RiverBank:
This is a dedicated repository for deployment application python on cluster Kubernetes

Tool we use :
 - Flask web server to display "hello world" code python3.8 
 - Vagrant to create a VMs for the cluster k8s and Terraform with libvirt because you use Terraform in your enterprise
 - K8s
 - Helm Chart for the deployment on K8S
 - Prometheus and Grafana for Monitoring

# Getting Started
Check version python

    python --version
    
Warning check if you have python3.7++
    [here](https://fr.wikihow.com/installer-Python)

Create a virtual environment

    python -m venv venv

Installation process

    pip install -r requirements.txt

Launch App

    python -m flask run --host=0.0.0.0

# Getting Started With Docker
Install Docker:

    curl -fsSL https://get.docker.com -o get-docker.sh

    sudo sh get-docker.sh

    sudo usermod -aG docker $USER

    newgrp docker

Launch container with app

    docker build --tag python-app-test .

    docker run python-app-test

# Static code analysis
Active your virtualenv

    source venv/bin/activate 

    or in windows: source venv/Script/activate

If you want exit of virtual environment

    deactivate

Install dependancies

    pip install -r dev-requirements.txt

Launch the test with Pytest, Bandit, Flake8 and Mypy

    bandit -r dss

    mypy dss

    flake8 dss

# Option Pull Dockerhub
Get image on Dockerhub

    docker pull adilmektoub06/test-python

    docker run adilmektoub06/test-python

