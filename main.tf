provider "libvirt" {
  uri = "qemu:///system"
}
resource "libvirt_volume" "centos7-qcow2" {
  name = "centos7.qcow2"
  pool = "default"
  source = "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2"
  format = "qcow2"
}
resource "libvirt_domain" "myvm" {
  name   = "myvm"
  memory = "1024"
  vcpu   = 1
  network_interface {
    network_name = "default"
  }
  disk {
    volume_id = libvirt_volume.centos7-qcow2.id
  }
  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }
  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}
output "ip" {
  value = "${libvirt_domain.myvm.network_interface.0.addresses.0}"
}

output "IPAddr" {
  value = element(virtualbox_vm.node.*.network_adapter.0.ipv4_address, 1)
}

output "IPAddr_2" {
  value = element(virtualbox_vm.node.*.network_adapter.0.ipv4_address, 2)
}